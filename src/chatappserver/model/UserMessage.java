package chatappserver.model;

public class UserMessage {

    public String messageContent;
    public String sender;
    public String receiver;
    public String createdAt;
    public String conversationId;

    public int dbId;
    public String messageType;

    public byte[] mediaData;
    public String filePath;
    public String fileType;
    public String hashValue;

}
