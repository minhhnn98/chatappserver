package chatappserver.model;

public class FileMetaInfoMessage {
    public String sender;
    public String receiver;
    public String conversationId;

    public int checksum;
    public String hashValue;
    public String fileType;
}
