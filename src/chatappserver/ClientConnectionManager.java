package chatappserver;

import chatappserver.binary.BinarySocket;
import chatappserver.model.FileMetaInfoMessage;
import chatappserver.model.UserMessage;
import chatappserver.textsocket.TextSocket;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ClientConnectionManager {

    String clientId;

    BinarySocket binarySocket;
    boolean isBinarySocketOpened = false;

    TextSocket textSocket;
    boolean isTextSocketOpened = false;

    List<UserMessage> textWaitingMessages;
    List<UserMessage> binaryWaitingMessages;

    public ClientConnectionManager(String clientId) {
        this.clientId = clientId;

        textWaitingMessages = new ArrayList<>();
        binaryWaitingMessages = new ArrayList<>();

        //new Thread(new RetrieveWaitingMessageRunnable()).start();
    }

    public void openTextSocket(Socket socket){
        textSocket = new TextSocket(socket, clientId);

        isTextSocketOpened = true;
        System.out.println("User " + clientId + " has connected with " + " text socket");

        /*//todo: synchronized messages by time
        new Thread(new Runnable() {
            @Override
            public void run() {
                for(int i=0;i<textWaitingMessages.size();i++){
                    try {
                        textSocket.sendTextToThisSocket(textWaitingMessages.get(i));
                        deleteSentMessage(textWaitingMessages.get(i).dbId);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();*/
    }


    public void openBinarySocket(Socket socket){
        binarySocket = new BinarySocket(socket, clientId);

        isBinarySocketOpened = true;
        System.out.println("User " + clientId + " has connected with " + " binary socket");

        //todo: synchronized messages by time
        new Thread(new Runnable() {
            @Override
            public void run() {
                for(int i=0;i<binaryWaitingMessages.size();i++){
                    try {
                        binarySocket.sendBinaryContentToThisSocket(binaryWaitingMessages.get(i));
                        deleteSentMessage(binaryWaitingMessages.get(i).dbId);
                        deleteFile(binaryWaitingMessages.get(i).filePath);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    public void sendText(UserMessage userMessage) throws IOException {
        textSocket.sendTextToThisSocket(userMessage);
    }

    public void sendBinary(UserMessage userMessage) throws IOException {
        binarySocket.sendBinaryContentToThisSocket(userMessage);
    }

    // send hash value to determine if file is needed to download
    public void sendFileMetaInfo(FileMetaInfoMessage metaInfoMessage) throws IOException {
        binarySocket.sendFileMetaInfoToThisSocket(metaInfoMessage);
    }

    public void sendRequestGetFile(FileMetaInfoMessage metaInfoMessage) throws IOException {
        binarySocket.sendGetRequestToThisSocket(metaInfoMessage);
    }

    public void sendRequestNotGetFile(FileMetaInfoMessage metaInfoMessage) throws IOException {
        binarySocket.sendNotGetRequestToThisSocket(metaInfoMessage);
    }

    public boolean isBinarySocketOpened() {
        return isBinarySocketOpened;
    }

    public void setBinarySocketOpened(boolean imageSocketOpened) {
        isBinarySocketOpened = imageSocketOpened;
        notifyConnectionBroken();
    }

    public boolean isTextSocketOpened() {
        return isTextSocketOpened;
    }

    public void setTextSocketOpened(boolean textSocketOpened) {
        isTextSocketOpened = textSocketOpened;
        notifyConnectionBroken();
    }

    private boolean isAllSocketsClosed(){
        return !isBinarySocketOpened && !isTextSocketOpened;
    }

    private void notifyConnectionBroken(){
        synchronized (ChatAppServer.activeConnection) {
            if (isAllSocketsClosed() && ChatAppServer.activeConnection.containsKey(clientId)) { {
                    ChatAppServer.activeConnection.remove(clientId);
                    System.out.println("User " + clientId + " has disconnected (all sockets are closed)");
                    disconnect();
                }
            }
        }
    }

    public class RetrieveWaitingMessageRunnable implements Runnable{

        @Override
        public void run() {
            String sql = "SELECT * "
                    + "FROM text_waiting WHERE receiver = ?";

            Connection conn = null;

            try {
                conn = DriverManager.getConnection(ChatAppServer.SQLITE_DB_PATH);
                PreparedStatement pstmt  = conn.prepareStatement(sql);

                // set the value
                pstmt.setString(1, clientId);

                ResultSet rs  = pstmt.executeQuery();

                // loop through the result set
                while (rs.next()) {
                    UserMessage userMessage = new UserMessage();
                    userMessage.sender =  rs.getString("sender");
                    userMessage.receiver = rs.getString("receiver");
                    userMessage.createdAt = rs.getString("timestamp");
                    userMessage.conversationId = rs.getString("conversationId");
                    userMessage.messageContent = rs.getString("content");
                    userMessage.mediaData = null;
                    userMessage.filePath = rs.getString("filePath");
                    userMessage.dbId = rs.getInt("id");

                    if(userMessage.messageContent != null){
                        textWaitingMessages.add(userMessage);
                    }
                    else if(userMessage.filePath != null){
                        binaryWaitingMessages.add(userMessage);
                    }

                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            if(conn!= null){
                try {
                    conn.close();
                } catch (SQLException throwable) {
                    throwable.printStackTrace();
                }
            }
        }
    }

    private boolean deleteFile(String filePath){
        File file = new File(filePath);

        if(file.exists()){
            return file.delete();
        }

        return false;
    }

    private void disconnect(){
        if(binarySocket != null){
            binarySocket.disconnect();
        }

        if(textSocket != null){
            textSocket.disconnect();
        }
    }

    private void deleteSentMessage(int id){
        String sql = "DELETE FROM text_waiting WHERE id = ?";

        try (Connection conn = DriverManager.getConnection(ChatAppServer.SQLITE_DB_PATH);
             PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the corresponding param
            pstmt.setInt(1, id);
            // execute the delete statement
            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

}
