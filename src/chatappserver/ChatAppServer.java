package chatappserver;

import chatappserver.model.FileMetaInfoMessage;
import chatappserver.model.UserMessage;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.*;
import java.util.HashMap;

public class ChatAppServer {

    static ServerSocket serverSocket;

    public static final int SERVER_PORT = 8000;

    public static final String TEXT_MESSAGE_TYPE = "text";
    public static final String BINARY_MESSAGE_TYPE = "binary";

    public static final String IMAGE_FILE_TYPE = "image";
    public static final String VIDEO_FILE_TYPE = "video";

    public static final String ACTION_REQUEST_GET_FILE = "action_request_get_file";
    public static final String ACTION_REQUEST_NOT_GET_FILE = "action_request_not_get_file";
    public static final String ACTION_SEND_FILE = "action_send_file";
    public static final String ACTION_SEND_FILE_META_INFO = "action_send_file_meta_info";

    public static final int BUFFER_SIZE = 8192;

    public static final String UID = "uid";
    public static final String DB_NAME = "chat_app_database.db";
    public static String SQLITE_DB_PATH = "jdbc:sqlite:" + System.getProperty("user.dir") + File.separator + DB_NAME;
    public static String FILE_STORAGE =  System.getProperty("user.dir") + File.separator + "file_storage";


    public static HashMap<String, ClientConnectionManager> activeConnection;


    public static void main(String[] args) {
        activeConnection = new HashMap<>();

        new Thread(new ServerThread()).start();
    }

    static class ServerThread implements Runnable {
        public void run() {
            try {
                serverSocket = new ServerSocket(SERVER_PORT);

                while (true) {
                    Socket clientSocket = serverSocket.accept();
                    new ClientConnection(clientSocket);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public static boolean isIdValid(String id){

        String sql = "SELECT * "
                + "FROM user_id WHERE id = ?";

        try (Connection conn = DriverManager.getConnection(ChatAppServer.SQLITE_DB_PATH);
             PreparedStatement pstmt  = conn.prepareStatement(sql)){

            pstmt.setString(1, id);
            ResultSet rs  = pstmt.executeQuery();

            if (!rs.isBeforeFirst()) {
                return false;
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return true;
    }

    public static void sendMessage(UserMessage userMessage){
        String type = userMessage.messageType;

        if(isIdValid(userMessage.receiver)){
            switch (type){
                case TEXT_MESSAGE_TYPE:
                    handleSendText(userMessage);
                    break;

                case BINARY_MESSAGE_TYPE:
                    handleSendBinary(userMessage);
                    break;
            }
        }
    }

    private static void handleSendText(UserMessage userMessage){
        if(activeConnection.containsKey(userMessage.receiver)){
            try {
                activeConnection.get(userMessage.receiver).sendText(userMessage);
            } catch (IOException e) {
                e.printStackTrace();
                insertWaitingTextMessageToDatabase(userMessage);
            }
        }
        else {
            insertWaitingTextMessageToDatabase(userMessage);
        }
    }

    public static void sendFileMetaInfo(FileMetaInfoMessage metaInfoMessage){
        if(activeConnection.containsKey(metaInfoMessage.receiver)){
            try {
                activeConnection.get(metaInfoMessage.receiver).sendFileMetaInfo(metaInfoMessage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //todo: receiver not currently connected
    }

    public static void sendRequestGetFile(FileMetaInfoMessage metaInfoMessage){
        if(activeConnection.containsKey(metaInfoMessage.sender)){
            try {
                activeConnection.get(metaInfoMessage.sender).sendRequestGetFile(metaInfoMessage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void sendRequestNotGetFile(FileMetaInfoMessage metaInfoMessage){
        if(activeConnection.containsKey(metaInfoMessage.sender)){
            try {
                activeConnection.get(metaInfoMessage.sender).sendRequestNotGetFile(metaInfoMessage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void handleSendBinary(UserMessage userMessage){
        if(activeConnection.containsKey(userMessage.receiver)){
            try {
                activeConnection.get(userMessage.receiver).sendBinary(userMessage);
            } catch (IOException e) {
                e.printStackTrace();
                insertWaitingImageMessageToDatabase(userMessage);
            }
        }
        else {
            insertWaitingImageMessageToDatabase(userMessage);
        }
    }

    static void insertWaitingImageMessageToDatabase(UserMessage userMessage){
        // save file to disk
        OutputStream fileOutputStream = null;

        String fileName = userMessage.sender + userMessage.receiver + "_" + userMessage.createdAt + ".jpeg";
        String filePath = FILE_STORAGE + File.separator + fileName;

        File file = new File(filePath);
        try {
            fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(userMessage.mediaData);
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        userMessage.filePath = filePath;

        // save info to database
        insertMessageToDatabase(userMessage);
    }

    static void insertWaitingTextMessageToDatabase(UserMessage userMessage){
        insertMessageToDatabase(userMessage);
    }

    static void insertMessageToDatabase(UserMessage userMessage){
        String sql = "INSERT INTO text_waiting(sender,receiver,content,conversationId,timestamp, filePath) VALUES(?,?,?,?,?,?)";

        try (Connection conn = DriverManager.getConnection(ChatAppServer.SQLITE_DB_PATH);
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, userMessage.sender);
            pstmt.setString(2, userMessage.receiver);
            pstmt.setString(3, userMessage.messageContent);
            pstmt.setString(4, userMessage.conversationId);
            pstmt.setString(5, userMessage.createdAt);
            pstmt.setString(6, userMessage.filePath);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
