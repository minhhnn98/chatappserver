package chatappserver;

import java.io.*;
import java.net.Socket;

import static chatappserver.ChatAppServer.*;

public class ClientConnection {

    Socket clientSocket;
    String id;

    private DataInputStream inputStream = null;


    public ClientConnection(Socket clientSocket) {
        this.clientSocket = clientSocket;

        try {
            inputStream = new DataInputStream(new BufferedInputStream(clientSocket.getInputStream()));

        } catch (IOException e) {
            e.printStackTrace();
        }

        new Thread(new ReadClientInformation()).start();
    }

    class ReadClientInformation implements Runnable{
        public void run() {
            String _id;
            String type;
            try {

                do{
                    _id = inputStream.readUTF();
                } while (!ChatAppServer.isIdValid(_id));

                type = inputStream.readUTF();

                id = _id;

                ClientConnectionManager clientConnectionManager = null;

                synchronized (activeConnection){

                if(activeConnection.containsKey(id)){
                    clientConnectionManager = activeConnection.get(id);
                }
                else {
                    clientConnectionManager = new ClientConnectionManager(id);
                }

                switch (type){
                    case TEXT_MESSAGE_TYPE:
                        clientConnectionManager.openTextSocket(clientSocket);
                        break;

                    case BINARY_MESSAGE_TYPE:
                        clientConnectionManager.openBinarySocket(clientSocket);
                        break;
                }

                    activeConnection.put(id, clientConnectionManager);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
