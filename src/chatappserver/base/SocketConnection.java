package chatappserver.base;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SocketConnection {

    protected Socket socket;
    protected MessageSender sender;
    protected MessageReceiver receiver;
    protected String id;

    public SocketConnection(Socket socket, String id) {
        this.socket = socket;
        this.id = id;

    }

    public void disconnect(){
        if(sender!= null){
            sender.disconnect();
        }

        if(receiver != null){
            receiver.disconnect();
        }
    }
}
