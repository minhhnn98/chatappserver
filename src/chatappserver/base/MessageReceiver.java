package chatappserver.base;

import chatappserver.model.FileMetaInfoMessage;
import chatappserver.model.UserMessage;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;

public class MessageReceiver {

    protected DataInputStream inputStream = null;
    protected Socket socket;
    protected String id;

    protected OnMessageReceivedListener messageReceivedCallback;

    public MessageReceiver(Socket socket, String id) {
        this.socket = socket;
        this.id = id;

        try {
            inputStream = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setOnMessageReceivedListener(OnMessageReceivedListener listener){
        this.messageReceivedCallback = listener;
    }

    public interface OnMessageReceivedListener{
        void onMessageReceived(UserMessage userMessage);
    }



    public void disconnect(){
        if(messageReceivedCallback != null){
            messageReceivedCallback = null;
        }

        try {
            if(socket != null) {
                    socket.close();
            }

            if(inputStream != null){
                    inputStream.close();
            }
        } catch (IOException e) {
                e.printStackTrace();
        }
    }

}
