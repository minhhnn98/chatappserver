package chatappserver.base;

import chatappserver.model.UserMessage;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.LinkedList;

public class MessageSender {

    protected DataOutputStream outputStream = null;
    protected Socket socket;

    protected boolean isRunning;

    protected LinkedList<UserMessage> tasks;

    public MessageSender(Socket socket) {
        this.socket = socket;
        tasks = new LinkedList<>();
        isRunning = true;

        try {
            outputStream = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }

       /* new Thread(new MessageSenderRunnable()).start();*/
    }


    public void disconnect(){
        try {
            if(socket != null) {
                socket.close();
            }

            if(outputStream != null){
               outputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addTask(UserMessage message){
        tasks.addLast(message);
    }


    /*class MessageSenderRunnable implements Runnable{

        @Override
        public void run() {
            while (isRunning){
                if(tasks.size() > 0){
                    chatappserver.model.UserMessage message = tasks.getFirst();
                    try {
                        sendMessage(message);
                        tasks.removeFirst();
                    } catch (IOException e) {
                        e.printStackTrace();
                        isRunning = false;
                    }
                }
            }
        }
    }

    private void sendMessage(chatappserver.model.UserMessage message) throws IOException {
        switch (message.type){
            case chatappserver.ClientConnection.TEXT_MESSAGE_TYPE:
                sendText(message);
                break;

            case chatappserver.ClientConnection.IMAGE_MESSAGE_TYPE:
                sendImage(message);
                break;
        }
    }*/






}
