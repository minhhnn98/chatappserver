package chatappserver.textsocket;

import chatappserver.ChatAppServer;
import chatappserver.model.UserMessage;
import chatappserver.base.MessageReceiver;
import chatappserver.base.SocketConnection;

import java.io.IOException;
import java.net.Socket;

public class TextSocket extends SocketConnection {
    public TextSocket(Socket socket, String id) {
        super(socket, id);

        sender = new TextSender(socket);
        receiver = new TextReceiver(socket, id);

        receiver.setOnMessageReceivedListener(new MessageReceiver.OnMessageReceivedListener() {
            @Override
            public void onMessageReceived(UserMessage userMessage) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        ChatAppServer.sendMessage(userMessage);
                    }
                }).start();
            }
        });
    }

    public void sendTextToThisSocket(UserMessage userMessage) throws IOException {
        synchronized (sender) {
            ((TextSender) sender).sendText(userMessage);
        }
    }
}
