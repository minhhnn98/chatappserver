package chatappserver.textsocket;

import chatappserver.base.MessageSender;
import chatappserver.model.UserMessage;

import java.io.IOException;
import java.net.Socket;

import static chatappserver.ChatAppServer.TEXT_MESSAGE_TYPE;

public class TextSender extends MessageSender {
    public TextSender(Socket socket) {
        super(socket);
    }

    public void sendText(UserMessage message) throws IOException {
        if(message == null || outputStream == null){
            return;
        }

        outputStream.writeUTF(TEXT_MESSAGE_TYPE);
        outputStream.flush();

        outputStream.writeUTF(message.conversationId);
        outputStream.writeUTF(message.sender);
        outputStream.writeUTF(message.createdAt);
        outputStream.writeUTF(message.messageContent);
        outputStream.flush();

        System.out.println("From: " + message.sender + " To: " + message.receiver + ": " + message.messageContent);
    }
}
