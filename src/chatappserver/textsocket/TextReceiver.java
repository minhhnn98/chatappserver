package chatappserver.textsocket;

import chatappserver.ChatAppServer;
import chatappserver.model.UserMessage;
import chatappserver.base.MessageReceiver;

import java.io.IOException;
import java.net.Socket;

public class TextReceiver extends MessageReceiver {

    public TextReceiver(Socket socket, String id) {
        super(socket, id);

        new Thread(new TextReceiverRunnable()).start();
    }

    private class TextReceiverRunnable implements Runnable{

        @Override
        public void run() {
            while (true) {
                try {
                    String messageType = inputStream.readUTF();
                    String receiver = inputStream.readUTF();
                    String conversationId = inputStream.readUTF();
                    String message = inputStream.readUTF();

                    UserMessage userMessage = new UserMessage();
                    userMessage.conversationId = conversationId;
                    userMessage.createdAt = String.valueOf(System.currentTimeMillis());
                    userMessage.mediaData = null;
                    userMessage.messageContent = message;
                    userMessage.receiver = receiver;
                    userMessage.sender = id;
                    userMessage.messageType = ChatAppServer.TEXT_MESSAGE_TYPE;

                    if(messageReceivedCallback != null){
                        messageReceivedCallback.onMessageReceived(userMessage);
                    }

                } catch (IOException e){
                    // user has disconnected
                    // Notes: may cause error (todo: fix later)
                    if(ChatAppServer.activeConnection.containsKey(id)){
                        try {
                            socket.close();
                        } catch (IOException ioException) {
                            ioException.printStackTrace();
                        }

                        ChatAppServer.activeConnection.get(id).setTextSocketOpened(false);
                        break;
                    }
                }
            }
        }
    }
}
