package chatappserver.binary;

import chatappserver.FileUtils;
import chatappserver.model.FileMetaInfoMessage;
import chatappserver.model.UserMessage;
import chatappserver.base.MessageSender;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;

import static chatappserver.ChatAppServer.*;

public class BinarySender extends MessageSender {

    public BinarySender(Socket socket) {
        super(socket);
    }

    public void sendBinaryFile(UserMessage message) throws IOException {
        if(outputStream == null || message.mediaData == null || message.mediaData.length == 0){
            return;
        }

        outputStream.writeUTF(ACTION_SEND_FILE);

        outputStream.writeUTF(message.conversationId);
        outputStream.writeUTF(message.sender);
        outputStream.writeUTF(message.hashValue);
        outputStream.writeUTF(message.fileType);
        outputStream.writeUTF(message.createdAt);
        outputStream.flush();

        int checkSum = message.mediaData.length;
        ArrayList<byte[]> chunks = divideByteArray(message.mediaData, BUFFER_SIZE);

        outputStream.writeInt(checkSum);
        outputStream.writeInt(chunks.size());
        outputStream.flush();

        for(int i=0;i<chunks.size();i++){
            outputStream.writeInt(chunks.get(i).length);
            outputStream.write(chunks.get(i),0, chunks.get(i).length);
            outputStream.flush();
            System.out.println("From: " + message.sender + " To: " + message.receiver + "#" + i +"(chunk) : " + chunks.get(i).length + " bytes");
        }

        System.out.println("From: " + message.sender + " To: " + message.receiver + " - Total: " + message.mediaData.length + " bytes");
    }

    public void sendFileMetaInfo(FileMetaInfoMessage metaInfoMessage) throws IOException {
        if(outputStream == null || metaInfoMessage.hashValue == null){
            return;
        }

        outputStream.writeUTF(ACTION_SEND_FILE_META_INFO);
        outputStream.flush();

        outputStream.writeUTF(metaInfoMessage.conversationId);
        outputStream.writeUTF(metaInfoMessage.sender);
        outputStream.writeUTF(metaInfoMessage.fileType);

        outputStream.writeUTF(metaInfoMessage.hashValue);
        outputStream.writeInt(metaInfoMessage.checksum);
        outputStream.flush();
    }

    public void sendRequestGetFile(FileMetaInfoMessage metaInfoMessage) throws IOException {
        if(socket == null || outputStream == null){
            return;
        }

        outputStream.writeUTF(ACTION_REQUEST_GET_FILE);
        outputStream.flush();

        outputStream.writeUTF(metaInfoMessage.conversationId);
        outputStream.writeUTF(metaInfoMessage.receiver);
        outputStream.writeUTF(metaInfoMessage.hashValue);
        outputStream.writeUTF(metaInfoMessage.fileType);
        outputStream.flush();
    }

    public void sendRequestNotGetFile(FileMetaInfoMessage metaInfoMessage) throws IOException {
        if(socket == null || outputStream == null){
            return;
        }

        outputStream.writeUTF(ACTION_REQUEST_NOT_GET_FILE);
        outputStream.flush();

        outputStream.writeUTF(metaInfoMessage.conversationId);
        outputStream.writeUTF(metaInfoMessage.receiver);
        outputStream.writeUTF(metaInfoMessage.hashValue);
        outputStream.writeUTF(metaInfoMessage.fileType);
        outputStream.flush();
    }

    private ArrayList<byte[]> divideByteArray(byte[] source, int chunkSize) {
        ArrayList<byte[]> result = new ArrayList<byte[]>();
        int start = 0;
        while (start < source.length) {
            int end = Math.min(source.length, start + chunkSize);
            result.add(Arrays.copyOfRange(source, start, end));
            start += chunkSize;
        }

        return result;
    }
}
