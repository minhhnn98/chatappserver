package chatappserver.binary;

import chatappserver.ChatAppServer;
import chatappserver.model.FileMetaInfoMessage;
import chatappserver.model.UserMessage;
import chatappserver.base.MessageReceiver;
import chatappserver.base.SocketConnection;

import java.io.IOException;
import java.net.Socket;

public class BinarySocket extends SocketConnection {
    public BinarySocket(Socket socket, String id) {
        super(socket, id);

        sender = new BinarySender(socket);
        receiver = new BinaryReceiver(socket, id);

        receiver.setOnMessageReceivedListener(new MessageReceiver.OnMessageReceivedListener() {
            @Override
            public void onMessageReceived(UserMessage userMessage) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        ChatAppServer.sendMessage(userMessage);
                    }
                }).start();
            }
        });

        ((BinaryReceiver)receiver).setOnBinaryFileReceivedListener(new BinaryReceiver.OnFileReceivedListener() {
            @Override
            public void onFileMetaInfoReceived(FileMetaInfoMessage fileInfo) {
                ChatAppServer.sendFileMetaInfo(fileInfo);
            }

            @Override
            public void onFileRequested(boolean isRequested, FileMetaInfoMessage metaInfoMessage) {
                if(isRequested){
                       ChatAppServer.sendRequestGetFile(metaInfoMessage);

                }else {
                    ChatAppServer.sendRequestNotGetFile(metaInfoMessage);
                }
            }
        });
    }

    public void sendGetRequestToThisSocket(FileMetaInfoMessage metaInfoMessage) throws IOException {
        ((BinarySender)sender).sendRequestGetFile(metaInfoMessage);
    }

    public void sendNotGetRequestToThisSocket(FileMetaInfoMessage metaInfoMessage) throws IOException {
        ((BinarySender)sender).sendRequestNotGetFile(metaInfoMessage);
    }

    public void sendBinaryContentToThisSocket(UserMessage userMessage) throws IOException {
        synchronized (sender) {
            ((BinarySender) sender).sendBinaryFile(userMessage);
        }
    }

    public void sendFileMetaInfoToThisSocket(FileMetaInfoMessage metaInfoMessage) throws IOException {
        synchronized (sender){
            ((BinarySender) sender).sendFileMetaInfo(metaInfoMessage);
        }
    }
}
