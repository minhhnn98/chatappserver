package chatappserver.binary;

import chatappserver.ChatAppServer;
import chatappserver.model.FileMetaInfoMessage;
import chatappserver.base.MessageReceiver;
import chatappserver.model.UserMessage;

import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.ArrayList;

public class BinaryReceiver extends MessageReceiver {

    protected OnFileReceivedListener binaryFileReceivedCallback;

    public BinaryReceiver(Socket socket, String id) {
        super(socket, id);
        new Thread(new BinaryReceiverRunnable()).start();
    }

    public class BinaryReceiverRunnable implements Runnable{

        @Override
        public void run() {
            while (true) {
                try {

                    String action = inputStream.readUTF();

                    handleAction(action);

                } catch (IOException e){
                    if(ChatAppServer.activeConnection.containsKey(id)){
                        try {
                            socket.close();
                            ChatAppServer.activeConnection.get(id).setBinarySocketOpened(false);
                        } catch (IOException ioException) {
                            ioException.printStackTrace();
                        }
                        break;
                    }
                }
            }
        }
    }

    private void handleAction(String action) throws IOException {
        switch (action){
            case ChatAppServer.ACTION_SEND_FILE_META_INFO:
                readFileMetaInfo();
                break;

            case ChatAppServer.ACTION_REQUEST_GET_FILE:
                readRequestGetFile();
                break;

            case ChatAppServer.ACTION_REQUEST_NOT_GET_FILE:
                readRequestNotGetFile();
                break;

            case ChatAppServer.ACTION_SEND_FILE:
                readIncomingFile();
                break;
        }
    }

    private void readRequestNotGetFile() throws IOException {
        FileMetaInfoMessage metaInfoMessage = new FileMetaInfoMessage();

        metaInfoMessage.conversationId = inputStream.readUTF();
        metaInfoMessage.sender = inputStream.readUTF();
        metaInfoMessage.receiver = inputStream.readUTF();
        metaInfoMessage.hashValue = inputStream.readUTF();
        metaInfoMessage.fileType = inputStream.readUTF();

        if(binaryFileReceivedCallback != null){
            binaryFileReceivedCallback.onFileRequested(false, metaInfoMessage);
        }

    }

    private void readIncomingFile() throws IOException {
        String conversationId = inputStream.readUTF();
        String receiver = inputStream.readUTF();
        String dataType = inputStream.readUTF();
        String hashValue = inputStream.readUTF();

        byte[] imageBytes;
        int checkSum = inputStream.readInt();
        int numbersOfChunk = inputStream.readInt();

        ArrayList<byte[]> chunks = new ArrayList<>();

        for(int i=0;i<numbersOfChunk;i++){
            int length = inputStream.readInt();
            imageBytes = new byte[length];
            inputStream.readFully(imageBytes, 0, length);
            chunks.add(imageBytes);
        }

        byte[] mergeArray = mergeByteArrays(chunks, checkSum);

        UserMessage userMessage = new UserMessage();
        userMessage.conversationId = conversationId;
        userMessage.createdAt = String.valueOf(System.currentTimeMillis());
        userMessage.mediaData = mergeArray;
        userMessage.messageContent = null;
        userMessage.receiver = receiver;
        userMessage.sender = id;
        userMessage.fileType = dataType;
        userMessage.messageType = ChatAppServer.BINARY_MESSAGE_TYPE;
        userMessage.hashValue = hashValue;

        if(messageReceivedCallback != null){
            messageReceivedCallback.onMessageReceived(userMessage);
        }
    }

    private void readRequestGetFile() throws IOException {
        FileMetaInfoMessage metaInfoMessage = new FileMetaInfoMessage();

        metaInfoMessage.conversationId = inputStream.readUTF();
        metaInfoMessage.sender = inputStream.readUTF();
        metaInfoMessage.receiver = inputStream.readUTF();
        metaInfoMessage.hashValue = inputStream.readUTF();
        metaInfoMessage.fileType = inputStream.readUTF();

        if(binaryFileReceivedCallback != null){
            binaryFileReceivedCallback.onFileRequested(true, metaInfoMessage);
        }
    }

    private void readFileMetaInfo() throws IOException {
        String conversationId = inputStream.readUTF();
        String receiver = inputStream.readUTF();
        String fileType = inputStream.readUTF();

        String hashValue = inputStream.readUTF();
        int checkSum = inputStream.readInt();

        FileMetaInfoMessage fileInfo = new FileMetaInfoMessage();
        fileInfo.sender = id;
        fileInfo.receiver = receiver;
        fileInfo.conversationId = conversationId;
        fileInfo.checksum = checkSum;
        fileInfo.hashValue = hashValue;
        fileInfo.fileType = fileType;

        if(binaryFileReceivedCallback != null) {
            binaryFileReceivedCallback.onFileMetaInfoReceived(fileInfo);
        }
    }

    @Override
    public void disconnect() {
        super.disconnect();

        if(binaryFileReceivedCallback != null){
            binaryFileReceivedCallback = null;
        }
    }

    private byte[] mergeByteArrays(ArrayList<byte[]> byteArrayList, int length){
        ByteBuffer result = ByteBuffer.allocate(length);
        for(int i=0;i<byteArrayList.size();i++){
            result.put(byteArrayList.get(i));
        }
        return result.array();
    }

    public void setOnBinaryFileReceivedListener(OnFileReceivedListener listener){
        this.binaryFileReceivedCallback = listener;
    }

    public interface OnFileReceivedListener {
        void onFileMetaInfoReceived(FileMetaInfoMessage fileInfo);
        void onFileRequested(boolean isRequested, FileMetaInfoMessage metaInfoMessage);
    }
}
